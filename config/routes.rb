Rails.application.routes.draw do
  devise_for :users

  devise_scope :user do
    get 'login', to: 'devise/sessions#new'
    root to: "devise/sessions#new"
  end

  get 'dispatcher/panel' => 'dispatchers#index'

  get 'search' => 'dispatchers#search'
  post 'search' => 'dispatchers#search'

  get 'change_order' => 'dispatchers#change_order'
  get 'dispatcher/icons' => 'dispatchers#icon'
  get 'render_icons' => 'map_markers#render_icons'

  post 'create_map_marker' => 'map_markers#create'
  get 'create_map_marker' => 'map_markers#create'

  get 'create_map_geo_object' => 'map_geo_objects#create'
  post 'create_map_geo_object' => 'map_geo_objects#create'

  get 'destroy_map_marker' => 'map_markers#destroy'

  #
  #

  get 'admin/home' => 'admins#index'
  get 'create_car' => 'admins#create_car'
  post 'create_car' => 'admins#create_car'

  post 'create_order' => 'orders#create'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
