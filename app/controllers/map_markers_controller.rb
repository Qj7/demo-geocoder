class MapMarkersController < ApplicationController

  def create
    @icon = MapMarker.new(icon: params[:file], name: (0...8).map { (65 + rand(26)).chr }.join)
    if @icon.save!
      respond_to do |format|
        format.json{ render :json => @icon }
      end
    end
  end

  def render_icons
    @markers = MapMarker.all
    respond_to do |format|
      format.js
    end
  end

  def destroy
    marker = MapMarker.find(params[:id])
    marker.destroy!
    @markers = MapMarker.all
    respond_to do |format|
      format.js
    end
  end

end