class AdminsController < ApplicationController
  before_action :authenticate_user!, :is_admin?
  def index

  end

  def create_car
    @car = Car.create car_params

    respond_to do |format|
      format.js
    end
  end

  private

  def car_params
    params.permit(:rate, :name)
        #.merge(status: current_user)
  end

  def is_admin?
    unless current_user && current_user.role == "admin"
      flash[:error] = "У вас недостаточно прав для просмотра!"
      redirect_to root_path
    end
  end
end