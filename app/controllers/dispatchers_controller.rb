class DispatchersController < ApplicationController
  before_action :authenticate_user!, :is_dispatcher?
  def index
=begin
    , bounds: [[47.961315, 37.813755], [48.128684, 38.110756]]
strictBounds: true
=end

=begin
    p results = Geocoder.search('макеевка Солнечный 11')
=end

=begin
    Geocoder.configure(lookup: :google)
    p results = Geocoder.search('донецк ильича 32', bounds: [[47.961315, 37.813755], [48.128684, 38.110756]])
=end
    @markers = MapMarker.all
    @objects = MapGeoObject.all
    @cars = Car.all
    @parkings = Parking.all
    @polygones = Polygon.all
  end

  def search
    if params[:term]
      p params[:term]
      @result = MapGeoObject.search(params[:term])
      @results = []
      @result.each do |result|
        @results << {latitude: result.latitude, longitude: result.longitude,
                     address: result.address, icon: 'base'}
      end

      Geocoder.configure(lookup: :yandex)
      results = Geocoder.search(params[:term], params: {results: 5})
      results.try(:each)do |res|
        if res.country == 'Украина'
          @results << {latitude: res.coordinates[0], longitude: res.coordinates[1],
                       address: res.address.split(', ').reverse.join(", "), icon: 'yandex'}
        end
      end


        response = HTTParty.get(URI.parse(URI.escape"http://openstreetmap.ru/api/search?q=#{params[:term]}"))
        response = JSON.parse(response)
        response['matches'].try(:each) do |res|
        @results << {latitude: res["lat"], longitude: res["lon"],
                     address: res["display_name"].split(', ').reverse.join(", "), icon: 'world'}
        end

        Geocoder.configure(lookup: :nominatim)
        results = Geocoder.search(params[:term], params: {language: :ru})
        results.try(:each)do |res|
          if res.country == 'Украина'
            @results << {latitude: res.coordinates[0], longitude: res.coordinates[1],
                         address: res.address, icon: 'world2'}
          end
        end

      respond_to do |format|
        format.js
      end

    end



  end

  def change_order
    car = Car.find(params["car_id"])
    car.set_list_position(params["position"])
    @parking = Parking.find(params["park_id"])

    respond_to do |format|
      format.js
    end
  end

  def icon
    @markers = MapMarker.all
  end

  private
  def is_dispatcher?
    unless current_user && current_user.role == "dispatcher"
      flash[:error] = "У вас недостаточно прав для просмотра!"
      redirect_to root_path
    end
  end


end
