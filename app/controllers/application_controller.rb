class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  def after_sign_in_path_for(resource_or_scope)
    if current_user.role == "dispatcher"
      dispatcher_panel_path
    elsif current_user.role == "driver"
      driver_home_path
    elsif current_user.role == "admin"
      admin_home_path
    end
  end
end
