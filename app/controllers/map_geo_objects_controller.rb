class MapGeoObjectsController < ApplicationController

  def create
    @object = MapGeoObject.create map_geo_object_params
    @objects = MapGeoObject.all
    @polygones = Polygon.all
    @markers = MapMarker.all
    respond_to do |format|
      format.js
    end
  end

  private

  def map_geo_object_params
    params.require(:map_geo_object).permit(:icon, :latitude, :longitude, :address)
  end

end