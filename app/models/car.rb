class Car < ActiveRecord::Base
  has_one :user
  belongs_to :parking
  acts_as_list
end
