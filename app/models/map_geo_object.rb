class MapGeoObject < ActiveRecord::Base
  include PgSearch
  pg_search_scope :search, against: :address, using: { tsearch: { any_word: true, prefix: true } }

end
