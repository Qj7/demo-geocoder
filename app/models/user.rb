class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  belongs_to :car
  devise :database_authenticatable,
         :trackable, :validatable

  validates :username, uniqueness: true

  def email_required?
    false
  end

  def email_changed?
    false
  end
end
