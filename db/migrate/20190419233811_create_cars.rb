class CreateCars < ActiveRecord::Migration
  def change
    create_table :cars do |t|
      t.string :name
      t.string :rate
      t.string :status
      t.integer :position
      t.float :latitude
      t.float :longitude
      t.belongs_to :parking, index: true
      t.timestamps null: false
    end
  end
end
