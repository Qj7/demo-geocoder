class AddCarToUsers < ActiveRecord::Migration
  def change
    add_reference :users, :car, index: true, foreign_key: true
  end
end
