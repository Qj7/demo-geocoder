class CreatePolygons < ActiveRecord::Migration
  def change
    create_table :polygons do |t|
      t.string :coordinates
      t.string :name
      t.string :color
      t.string :cost
      t.timestamps null: false
    end
  end
end
