class CreateMapMarkers < ActiveRecord::Migration
  def change
    create_table :map_markers do |t|
      t.string :name
      t.string :icon
      t.timestamps null: false
    end
  end
end
