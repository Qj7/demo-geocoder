class CreateOrders < ActiveRecord::Migration
  def change
    create_table :orders do |t|
      t.string :park
      t.string :address
      t.string :tarif
      t.string :status
      t.boolean :online?
      t.references :car
      t.timestamps null: false
    end
  end
end
