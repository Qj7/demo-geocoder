class CreateMapGeoObjects < ActiveRecord::Migration
  def change
    create_table :map_geo_objects do |t|
      t.string :icon
      t.float :latitude
      t.float :longitude
      t.string :address
      t.timestamps null: false
    end
  end
end

